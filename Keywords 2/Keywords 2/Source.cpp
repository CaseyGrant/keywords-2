// Hangman
// The classic game of hangman

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cctype>

using namespace std;

int simulations = 1; // Create an int var to count the number of simulations being run starting at 1
string name; //Hold the recruit's name in a var
int numOfWords = 0; // sets how many times you run through before starting again
const int MAX_WRONG = 8; //maximum number of incorrect guesses allowed
int wrong = 0; //number of incorrect guesses
string progress; // used if you want to start again
vector<string> words; //collection of possible words to guess
void simulation(); // runs most of the code


int main()
{
	// Create a collection of 10 words you had written down manually
	words.push_back("SUBSTANTIATION");
	words.push_back("PEREMPTORY");
	words.push_back("PERSONABLE");
	words.push_back("TARNISHED");
	words.push_back("PECCADILLO");
	words.push_back("EXIGENCY");
	words.push_back("QUANDARY");
	words.push_back("AMALGAMATE");
	words.push_back("DISSEMINATE");
	words.push_back("MULTIFARIOUS");


	cout << "Please log in using your name.\n"; // Ask the recruit to log in using their name
	cin >> name;// and address them by it throughout the simulation.

	cout << "Welcome to hangman " << name << ". Good luck!\n"; // Display Title of the program to the user

	cout << "Here's what this program is all about\n";
	cout << "The current automated programs that the CIA uses to decode enemy transmissions have always worked well until recently. \nRecently the enemy is using single scrambled low-tech keywords to signal other enemies to start attacks \nwhich our current code decryption programs have not been successful against.\nAt the CIA they are developing a new codebreaking team that relies on human expertise \nto detect these low tech scrambled keywords. We are turning back the clock with this new team of human code breakers.\n"; // Display an overview of what Keywords II is to the recruit 
	simulation(); // runs another function

	return 0;
}

void simulation()
{
	// Pick new 3 random words from your collection as the secret code word the recruit has to guess. 
	do
	{
		cout << "This is simulation #" << simulations << "\n"; // Display the simulation # is starting to the recruit. 
		cout << "Type in letters to try to guess the word.\n"; // Display directions to the recruit on how to use Keywords

		srand(static_cast<unsigned int> (time(0))); // creates a random seed based on the time
		random_shuffle(words.begin(), words.end()); // shuffles the word

		const string THE_WORD = words[0]; //word to guess
		string soFar(THE_WORD.size(), '-'); //word guessed so far
		string used = ""; //letters already guessed

		// While recruit hasn�t made too many incorrect guesses and hasn�t guessed the secret word
		while ((wrong < MAX_WRONG) && (soFar != THE_WORD))//main loop
		{
			cout << "\n\n" << name << " you have " << (MAX_WRONG - wrong) << " incorrect guesses left.\n"; //Tell recruit how many incorrect guesses he or she has left
			cout << "\nYou've used the following letters:\n" << used << endl; //Show recruit the letters he or she has guessed
			cout << "\nSo far, the word is:\n" << soFar << endl; //     Show player how much of the secret word he or she has guessed

			// Get recruit's next guess
			char guess;
			cout << "\n\nEnter your guess: ";
			cin >> guess;
			system("CLS");
			guess = toupper(guess); //make uppercase since secret word in uppercase

			// While recruit has entered a letter that he or she has already guessed
			while (used.find(guess) != string::npos)
			{
				//Get recruit �s guess
				cout << "\nYou've already guessed " << guess << endl;
				cout << "Enter your guess: ";
				cin >> guess;
				system("CLS");
				guess = toupper(guess); // Add the new guess to the group of used letters
			}

			used += guess;

			// If the guess is in the secret word
			if (THE_WORD.find(guess) != string::npos)
			{
				cout << "That's right! " << guess << " is in the word.\n"; // Tell the recruit the guess is correct
				//update soFar to include newly guessed letter
				for (int i = 0; i < THE_WORD.length(); ++i) // Update the word guessed so far with the new letter
				{
					if (THE_WORD[i] == guess)
					{
						soFar[i] = guess;
					}
				}
			}
			else // Otherwise
			{
				cout << "Sorry, " << guess << " isn't in the word.\n"; // Tell the recruit the guess is incorrect
				++wrong; // Increment the number of incorrect guesses the recruit has made
			}
		}

		// If the recruit has made too many incorrect guesses
		if (wrong == MAX_WRONG)
		{
			cout << "\nYou've been hanged!"; // Tell the recruit that he or she has failed the Keywords II course.
		}
		else // Otherwise
		{
			cout << "\nYou guessed it!"; // Congratulate the recruit on guessing the secret words
		}

		cout << "\nThe word was " << THE_WORD << endl;
		numOfWords++;
		if (numOfWords < 3)
		{
			cout << "Here's a new word! There are " << (3 - numOfWords) << " words left in this simulation\n"; // tells the user how many words are left in the current sim
		}

	} while (numOfWords < 3);

	// Ask the recruit if they would like to run the simulation again

	cout << "Would you like to run the simulation again?\n";
	cin >> progress;

	// If the recruit wants to run the simulation again
	if (progress == "yes")
	{
		simulations++; // Increment the number of simulations ran counter
		system("CLS");
		simulation(); //     Move program execution back up to // Display the simulation # is starting to the recruit. 
	}
	else // Otherwise 
	{
		cout << "End of Simulations"; // Display End of Simulations to the recruit
	}

	system("pause"); // Pause the Simulation with press any key to continue
}